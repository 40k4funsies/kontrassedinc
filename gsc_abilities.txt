GSC abilities

CULT AMBUSH
Genestealer Cults plan meticulously before rising up against their oppressors, remaining hidden until the moment of ascension arrives. During deployment, you can set up this unit in ambush instead of on the battlefield. If this unit has the INFANTRY or BIKER keyword, you can either set it up in ambush or underground instead of on the battlefield. When you set up a unit underground, it can emerge at the end of any of your Movement phases – set the unit up anywhere on the battlefield that is more than 9" from any enemy models. When you set up a unit in ambush, place one ambush marker anywhere on the battlefield that is wholly within your deployment zone. You will need one ambush marker for each unit that will deploy in this way. If you set up a TRANSPORT in ambush, you must still tell your opponent what units are embarked within it when it is set up in ambush – do not set up separate ambush markers for units that start the battle embarked within a TRANSPORT, even if they have the Cult Ambush ability. Ambush markers are not units and cannot be targeted, attacked or destroyed. When measuring to or from ambush markers, always measure to the centre of the marker. If you are playing a mission that uses Concealed Deployment, the Concealed Deployment rules only apply to units that do not have the Cult Ambush ability. If you are playing a mission that uses Sentries, Sentry models cannot be set up in ambush, even if they have the Cult Ambush ability. Matched Play: In matched play, units set up in ambush using this rule do not count as being Strategic Reserves or Reinforcement units.

Revealing Ambush Markers
If you have the first turn, you must reveal all of your ambush markers at the start of your Movement phase, one at a time, before moving any units. Each time you reveal an ambush marker, select one unit from your army that you set up in ambush, then set up one model from that unit within 1" of that ambush marker. Then remove that marker before setting up the rest of that model’s unit wholly within 6" of the first model, wholly within your deployment zone and more than 9" from any enemy models (any models that cannot be placed are destroyed). If it is your turn, that unit can still move and shoot normally  during the turn it is set up, but if it is a TRANSPORT , units that disembark from it this turn cannot be set up within 9" of any enemy models. Note that unless these units actually move during this Movement phase, they do not count as having moved in their Movement phase for any rules purposes, such as shooting Heavy weapons. If your opponent has the first turn, then none of their units can be set up or end a move within 9" of any of your ambush markers. At the end of your opponent’s first Movement phase, after they have set up all of their units from reinforcements (if any), reveal all of your ambush markers as described above before continuing with the turn.

UNQUESTIONING LOYALTY
So fanatically devoted are Genestealer Cultists that they would die for their masters. Each time you fail a saving throw for a <CULT> CHARACTER model, and each time a <CULT> CHARACTER model suffers a mortal wound, before inflicting damage check to see if it is within 3" of any friendly <CULT> or BROOD BROTHERS units with this ability. If it is, you can select one of those units and roll a D6; on a 4+ you do not inflict any damage on the character, but one model in the selected unit (your choice) is slain. Otherwise, the character suffers damage as normal.

BATTLE-FORGED ABILITIES
If your army is Battle-forged, all Troops units in GENESTEALER CULTS and BROOD BROTHERS Detachments gain the Insurrectionists ability, and all GENESTEALER CULTS Detachments gain the Cult Creed ability.

INSURRECTIONISTS
A unit with this ability that is within range of an objective marker (as specified in the mission) controls the objective marker even if there are more enemy models within range of that objective marker. If an enemy unit within range of the same objective marker has a similar ability, then the objective marker is controlled by the player who has the most models within range of it as normal.

CREEDS

CULT OF THE FOUR-ARMED EMPEROR: SUBTERRANEAN AMBUSHERS
Until the end of the first battle round, add 1 to Advance and charge rolls made for units with this Cult Creed. Starting from the second battle round, if a unit with this Cult Creed is set up on the battlefield, then until the end of that turn, add 1 to Advance and charge rolls made for that unit.

THE PAUPER PRINCES: DEVOTED ZEALOTS
You can re-roll hit rolls for attacks made with melee weapons by a unit with this Cult Creed in a turn in which it made a charge move, was charged or performed a Heroic Intervention.

THE HIVECULT: DISCIPLINED MILITANTS
Each time a Morale test is failed for a unit with this Cult Creed, until the end of the phase, halve the number of models that flee that unit due to failed Combat Attrition tests (rounding fractions down). In addition, units with this Cult Creed can still shoot in a turn in which they Fall Back, but if they do so you must subtract 1 from their hit rolls in the Shooting phase of that turn.

THE BLADED COG: CYBORGISED HYBRIDS
All models with this Cult Creed have a 6+ invulnerable save. Models with this Cult Creed that already have an invulnerable save instead improve their invulnerable save by 1 (to a maximum of 3+). In addition, INFANTRY models with this Cult Creed do not suffer the penalty to their hit rolls for moving and shooting Heavy weapons.

THE RUSTED CLAW: NOMADIC SURVIVALISTS
When making saving throws (excluding invulnerable saving throws) for a model with this Cult Creed, add 1 to the result if the weapon being used to make the attack has an Armour Penetration characteristic of 0 or -1. If a Biker unit with this Cult Creed Advances, until the end of the turn, all models in that unit treat all Pistol and Rapid Fire weapons they are equipped with as Assault weapons. In addition, Biker models with this Cult Creed do not suffer the penalty to their hit rolls for Advancing and shooting Assault weapons.

THE TWISTED HELIX: EXPERIMENTAL SUBJECTS 
Add 1 to the Strength characteristic of models with this Cult Creed. In addition, add 2 to Advance rolls for a unit with this Cult Creed.
